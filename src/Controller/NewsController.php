<?php

namespace App\Controller;

use App\Service\News\NewsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class NewsController extends AbstractController
{
    private $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }


    /**
     * @Route("/news", name="short_news_list")
     */
    public function getShortNews(Request $request): JsonResponse{
        $page = (int) $request->query->get("page");
        $count = (int) $request->query->get("count");

        $news = $this->newsService->getNews($page,$count);

        return new JsonResponse($news,Response::HTTP_OK);
    }

    /**
     * @Route("/news/{id}", name="get_full_news_by_id", methods={"GET","HEAD"})
     */
    public function getNewsById(Request $request,int $id): JsonResponse{
        $news = $this->newsService->getNewsById($id);

        return new JsonResponse($news,Response::HTTP_OK);
    }

    public function downRating(Request $request,int $id): JsonResponse{
        return new JsonResponse($this->newsService->downRatingNews($id),Response::HTTP_OK);
    }

    public function upRating(Request $request,int $id): JsonResponse{
        return new JsonResponse($this->newsService->upRatingNews($id),Response::HTTP_OK);
    }

    /**
     * @Route("/news/{id}", name="delete_news_by_id", methods={"DELETE"})
     */
    public function deleteNews(Request $request,int $id): JsonResponse{
        return new JsonResponse($this->newsService->deleteNewsById($id),Response::HTTP_NO_CONTENT);
    }

}