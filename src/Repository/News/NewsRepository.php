<?php

namespace App\Repository\News;

use App\Entity\News;

interface NewsRepository
{
    public function getNewsByPageAndCount(int $page = 1,int $count = 30): array;
    public function getById(int $id): ?News;
    public function save(News $news): News;
    public function delete(News $news): bool;
}