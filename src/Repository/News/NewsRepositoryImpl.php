<?php

namespace App\Repository\News;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<News>
 *
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepositoryImpl extends ServiceEntityRepository implements NewsRepository
{
    private const MIN_PAGE = 1;
    private const MAX_COUNT = 150;


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function getNewsByPageAndCount(int $page = 1, int $count = 30): array
    {
        $count = ($count > self::MAX_COUNT) ? self::MAX_COUNT : $count;
        $page = ($page <= 0 ) ? self::MIN_PAGE : $page;

        return $this->createQueryBuilder("n")
            ->setFirstResult($count*($page-1))
            ->setMaxResults($count)
            ->orderBy("n.createdAt","desc")
            ->getQuery()
            ->getResult();
    }

    public function save(News $news): News
    {
       $em = $this->getEntityManager();

       $em->persist($news);
       $em->flush();

       return $news;
    }

    public function delete(News $news): bool
    {
        try{
            $em = $this->getEntityManager();
            $em->remove($news);
            $em->flush();
            return true;
        } catch (\Exception $e){
            return false;
        }
    }

    public function getById(int $id): ?News
    {
        return $this->createQueryBuilder("n")
            ->where("n.id",$id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
