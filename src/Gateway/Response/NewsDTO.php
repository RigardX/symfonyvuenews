<?php

namespace App\Gateway\Response;

class NewsDTO
{
    public string $title;
    public \DateTimeImmutable $createdAt;
    public string $description;
    public ?string $imageUrl;

    /**
     * @param string $title
     * @param \DateTimeImmutable $createdAt
     * @param string $description
     * @param string|null $imageUrl
     */
    public function __construct(string $title, \DateTimeImmutable $createdAt, string $description, ?string $imageUrl = "")
    {
        $this->title = $title;
        $this->createdAt = $createdAt;
        $this->description = $description;
        $this->imageUrl = $imageUrl;
    }


}