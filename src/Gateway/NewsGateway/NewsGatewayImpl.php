<?php

namespace App\Gateway\NewsGateway;

use App\Gateway\BaseGateway;
use App\Gateway\Response\NewsDTO;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NewsGatewayImpl extends BaseGateway implements NewsGateway
{
    private const FROM = ["rbc"];
    private const PARSE_MAP = [
        "rbc" => [
            "mainUrl" => "https://www.rbc.ru/short_news",
            "newsObj" => ".item__link",
            "patterns" => [
                "www.rbc.ru" => [
                    "title" => ".article__header__title-in",
                    "createdAt" => ".article__header__date",
                    "description" => ".article__text_free",
                    "image" => ".article__main-image__wrap img"
                ]
            ]
        ],
    ];

    public function __construct(HttpClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * @param string $from
     * @param int $count
     * @return NewsDTO[]
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function parseNews(string $from, int $count = 15): array
    {
        $resp = [];

        if (in_array($from, self::FROM)){
            $mapObj = self::PARSE_MAP[$from];
            $res = $this->client->request("GET",$mapObj["mainUrl"]);
            $content = $res->getContent();

            $crawler = new Crawler($content);

            $i=0;

            while($i < $count){
                try{
                    $url = $crawler->filter($mapObj["newsObj"])->getNode($i)->attributes[0]->value;
                    $resp[] = $this->parseFullNews($url,$from);
                } catch (\Throwable $e){
                    echo "error";
                }
                $i++;
            }

            return $resp;

        } else throw new \Exception("parse object not found");
    }


    /**
     * @param string $fromUrl
     * @param string $type список указанных типов перечисленны в константе self::FROM
     * @return NewsDTO
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function parseFullNews(string $fromUrl,string $type){
        $mapObj = self::PARSE_MAP[$type];
        $keyHost = parse_url($fromUrl)["host"];
        if (array_key_exists($keyHost,$mapObj["patterns"])){
            $patternObj = $mapObj["patterns"][$keyHost];

            $res = $this->client->request("GET",$fromUrl);
            $content = $res->getContent();

            $crawler = new Crawler($content);

            $title = $crawler->filter($patternObj["title"])->text();
            $createdAt = new \DateTimeImmutable($crawler->filter($patternObj["createdAt"])->attr("datetime"));
            $description = $crawler->filter($patternObj["description"])->text();
            $image = $crawler->filter($patternObj["image"])->attr("src");

            return new NewsDTO($title,$createdAt,$description,$image);

        } else throw new \Exception("pattern not found,parse not possible");
    }

}