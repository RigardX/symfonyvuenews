<?php

namespace App\Gateway\NewsGateway;

interface NewsGateway
{
    public function parseNews(string $from,int $count = 15): array;
}