<?php

namespace App\Gateway;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class BaseGateway
{
    protected $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
}