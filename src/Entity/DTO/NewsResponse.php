<?php

namespace App\Entity\DTO;

class NewsResponse
{
    public int $id;
    public string $title;
    public string $description;
    public string $imageUrl;
    public int $rating;
    public \DateTimeImmutable $createdAt;

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @param string $imageUrl
     * @param int $rating
     * @param \DateTimeImmutable $createdAt
     */
    public function __construct(int $id, string $title, string $description, string $imageUrl, int $rating, \DateTimeImmutable $createdAt)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->imageUrl = $imageUrl;
        $this->rating = $rating;
        $this->createdAt = $createdAt;
    }


}