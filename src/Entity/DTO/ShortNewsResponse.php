<?php

namespace App\Entity\DTO;

class ShortNewsResponse
{
    public int $id;
    public string $title;
    public string $description;
    public int $rating;
    public string $fullNewsUrl;
    public \DateTimeImmutable $createdAt;

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @param int $rating
     * @param string $fullNewsUrl
     * @param \DateTimeImmutable $createdAt
     */
    public function __construct(int $id, string $title, string $description, int $rating, string $fullNewsUrl, \DateTimeImmutable $createdAt)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = substr($description,0,200);
        $this->rating = $rating;
        $this->fullNewsUrl = $fullNewsUrl;
        $this->createdAt = $createdAt;
    }


}