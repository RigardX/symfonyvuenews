<?php

namespace App\Service\News;

use App\Gateway\Response\NewsDTO;

interface NewsService
{
    public function createNews(NewsDTO $newsDTO);
    public function getNews(int $page = 1,int $count = 30);
    public function getNewsById(int $idNews);
    public function upRatingNews(int $idNews);
    public function downRatingNews(int $idNews);
    public function deleteNewsById(int $idNews);
}