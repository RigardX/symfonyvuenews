<?php

namespace App\Service\News;

use App\Entity\DTO\NewsResponse;
use App\Entity\DTO\ShortNewsResponse;
use App\Entity\News;
use App\Gateway\Response\NewsDTO;
use App\Repository\News\NewsRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsServiceImpl implements NewsService
{
    private $newsRepository;
    private $newsFactory;

    public function __construct(NewsRepository $newsRepository,NewsFactory $newsFactory)
    {
        $this->newsRepository = $newsRepository;
        $this->newsFactory = $newsFactory;
    }

    public function createNews(NewsDTO $newsDTO): NewsResponse
    {
        $news = $this->newsFactory->make(
            $newsDTO->title,
            $newsDTO->description,
            $newsDTO->imageUrl,
            $newsDTO->createdAt
        );

        $news = $this->newsRepository->save($news);

        return new NewsResponse(
            $news->getId(),
            $news->getTitle(),
            $news->getDescription(),
            $news->getImageUrl(),
            $news->getRating(),
            $news->getCreatedAt());
    }

    public function getNews(int $page = 1, int $count = 30)
    {
        $newsArray = $this->newsRepository->getNewsByPageAndCount($page,$count);

        $resp = [];

        /** @var News $news */
        foreach ($newsArray as $news){
            $resp[] = new ShortNewsResponse(
                $news->getId(),
                $news->getTitle(),
                $news->getDescription(),
                $news->getRating(),
                "",
                $news->getCreatedAt()
            );
        }

        return $resp;
    }

    public function upRatingNews(int $idNews) : NewsResponse
    {
        $news = $this->newsRepository->getById($idNews);

        if ($news == null){
            throw new NotFoundHttpException();
        }

        $news->setRating($news->getRating()+1);

        $news = $this->newsRepository->save($news);

        return new NewsResponse(
            $news->getId(),
            $news->getTitle(),
            $news->getDescription(),
            $news->getImageUrl(),
            $news->getRating(),
            $news->getCreatedAt());
    }

    public function downRatingNews(int $idNews) : NewsResponse
    {
        $news = $this->newsRepository->getById($idNews);

        if ($news == null){
            throw new NotFoundHttpException();
        }

        $news->setRating($news->getRating()-1);

        $news = $this->newsRepository->save($news);

        return new NewsResponse(
            $news->getId(),
            $news->getTitle(),
            $news->getDescription(),
            $news->getImageUrl(),
            $news->getRating(),
            $news->getCreatedAt());
    }

    public function getNewsById(int $idNews)
    {
        $news = $this->newsRepository->getById($idNews);

        if ($news == null){
            throw new NotFoundHttpException();
        }

        return new NewsResponse(
            $news->getId(),
            $news->getTitle(),
            $news->getDescription(),
            $news->getImageUrl(),
            $news->getRating(),
            $news->getCreatedAt());
    }

    public function deleteNewsById(int $idNews): NewsResponse
    {
        $news = $this->newsRepository->getById($idNews);

        if ($news == null){
            throw new NotFoundHttpException();
        }

        return new NewsResponse(
            $news->getId(),
            $news->getTitle(),
            $news->getDescription(),
            $news->getImageUrl(),
            $news->getRating(),
            $news->getCreatedAt());
    }
}