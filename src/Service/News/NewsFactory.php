<?php

namespace App\Service\News;

use App\Entity\News;

class NewsFactory
{
    public function make(string $title,string $description,string $imageUrl,\DateTimeImmutable $createdAt) : News{
        $news = new News();
        $news->setTitle($title)
            ->setDescription($description)
            ->setCreatedAt($createdAt)
            ->setImageUrl($imageUrl)
            ->setRating(rand(0,10));

        return $news;
    }
}