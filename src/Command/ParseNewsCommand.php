<?php

namespace App\Command;

use App\Gateway\NewsGateway\NewsGateway;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseNewsCommand extends Command
{
    private $newsGateway;
    protected static $defaultName = 'app:parse-news';

    public function __construct(NewsGateway $newsGateway)
    {
        $this->newsGateway = $newsGateway;

        parent::__construct();
    }

    protected function configure()
    {
       $this->addArgument("from",InputArgument::REQUIRED,"откуда будет скачиваться новости, пока доступно только rbk");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $from = $input->getArgument("from");

        $this->newsGateway->parseNews($from);

        return Command::SUCCESS;
    }

}