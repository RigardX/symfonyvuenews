import Vue, {createApp} from 'vue';

import App from './views/App';
import router from "./routes.js";

const app = createApp(App)
    .use(router)
    .mount('#app')

export default app;