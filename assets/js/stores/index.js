import {createStore} from "vuex";

const store = createStore({
    state:{
        expireSecondsToUpdate:1,
        resultsPerPage:15
    },
    mutations:{
        setExpireSecondsToUpdate(state,payload){
            state.expireSecondsToUpdate = payload;
        },
        setResultPerPage(state,payload){
            state.resultsPerPage = payload;
        },
    },
    actions:{
        updateExpireSecondsToUpdate({commit},payload){
            commit("setExpireSecondsToUpdate",payload);
        },
        updateResultPerPage({commit},payload){
            commit("setResultPerPage",payload);
        }
    },
    modules:{

    }
})