import {createMemoryHistory, createRouter} from 'vue-router';
import App from "./views/App";


const router = createRouter(({
    history: createMemoryHistory(),
    routes: [
        {path:'/', name:'home', component:App}
    ]
}))

export default router;